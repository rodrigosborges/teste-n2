<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Teste-n2">
  <meta name="author" content="Rodrigo Borges">

  <title>Slice TI</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  
  <!-- Slice TI Icon -->      
  <link rel="shortcut icon" href="http://slice-ti.com.br/site/wp-content/uploads/2014/05/Logotipo_Slice_TI_Cubo_Transparente.png">
  
  <!-- Custom styles for this template -->
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
  <link href="css/blog.css" rel="stylesheet">

</head>